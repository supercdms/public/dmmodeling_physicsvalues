# DM Modeling PhysicsValues Package

Repository and python package for constants and other physics values used for dark matter modeling. The constants and parameters are stored in a language independent way. 

This package is made available by the SuperCDMS Collaboration to provide in the public domain the software used for SuperCDMS studies. Though validated by the Collaboration, there is no warranty or guarantee that it is correct. Users should perform their own validation of the functions contained within this package.

Contacts - Taylor Aralis (taralis@caltech.edu), Matt Wilson (matthew.wilson@kit.edu), Alexander Zaytsev (alexander.zaytsev@kit.edu)

---

## Table of Contents
  1. [Introduction](#intro)
  2. [Installation](#install)
      1. [Install Dependencies](#install_dep)
  3. [Data Organization](#data_org)
  4. [Usage](#use)
  5. [Project Status](#status)
  6. [Issue Tracking](#issue_tracking)

---

## Introduction <a name="intro"></a>
This repository and python package is used to store various constants and parameters that are necessary at various stages of an analysis (for example, generating DM signal models) and are common across multiple analyses. The values themselves, as well as other relevant information, are stored in a xml file so that they can be retrieved in a language independent way. The python script `physics_values_loader.py` contains various classes and functions to load or read the constants in the xml file. 

---

## Installation <a name="install"></a>
#### Install Dependencies: <a name="install_dep"></a>
-  numpy

This git repository is also an installable python package. There are various ways to install a python package, but a common way is to first git clone the repository into your local directory. Once that is done, the package can be installed as follows:
```
pip install [path-to-dmmodeling_physicsvalues-package]/dmmodeling_physicsvalues
```
or
```
pip install --upgrade [path-to-dmmodeling_physicsvalues-package]/dmmodeling_physicsvalues
```
Depending on the user's setup, the installation may or may not be done within a python venv. In some cases, it may be necessary to add the `--user` argument to the pip install command. If the package is installed from a Juptyer notebook, restart the kernal after the package is successfully installed before importing.

***Package installation works with pip version 21.3.1. Try updating pip if the installation fails.***

---

## Data Organization <a name="data_org"></a>
The constants and parameters in the xml file called "constants.xml" are organized in a specific way using nested elements. Specifically, the xml file consists of **categories** which are parent elements, and within each category are the constants/parameters belonging to that category and/or child elements with their own constants or parameters:

- category 1
  - constant/parameter a
  - constant/parameter b
  - ...
- category 2
  - constant/parameter a
  - constant/parameter b
  - child element a
    - constant/parameter a
  - ...
- ...

Each constant/parameter element in the xml file has itself additional child elements that contain the information corresponding to that constant or parameter. Every constant/parameter element has the child element `<value>`, which stores the value of the constant or parameter. The `<value>` element in most cases includes the attribute `type="float"`, which indicates to the loader function that this element needs to be converted to a float object. In cases where the constant/parameter is an array (for example, a covariance matrix), the `<value>` element has the attributes `type="array" shape="n, m" `, where `n` and `m` are the dimensions of the array.

Each constant/parameter element can also contain additional optional child elements that contain additional information. These may include:

- `<units>`: units of the constant/parameter, if existing
- `<unc>`: uncertainty of the constant/parameter, if existing
- `<source>`: source for the constant/parameter, if existing
- `<info>`: additional information for the constant/parameter, if the constant/paramter name is not clear enough.

For the child elements whose value is a number (for example with the `<unc>` child element), the attribute `type="float"` is added.

For example, the parameter "Eg10" for Si is the 0 K energy of the first indirect band gap. Because it is a parameter specifically for Si, it exists under the "Si_params"  and "bandgap_params" parent elements. Furthermore, this particular parameter has uncertainty associated with it. Therefore in the xml file it will appear as:

```xml
<constants>
    <Si_params>
        <bandgap_params>
            <Eg10>
                <value type="float">1.131</value>
                <units>eV</units>
                <unc type="float">0.003</unc>
                <info>Bandgap energy of first indirect bandgap at 0K. Covariance matrix is at c0_c1_A2_Eg10_covar</info>
                <source>https://arxiv.org/pdf/2010.15844.pdf (TODO: change to HVeV-R3, when it's out)</source>
            </Eg10>
        </bandgap_params>
    </Si_params>
</constants>

```

A simpler example is the speed of light constant, which in the xml file will appear as:

```xml
<constants>
    <physics_params>
        <speed_of_light>
            <value type="float">29979245800.0</value>
            <units>cm/s</units>
        </speed_of_light>
    </physics_params>
</constants>

```

---

## Usage <a name="use"></a>
The constants and parameters stored in the xml file can be read directly by a user in whichever programming language they are using. However, this package contains python functions in order to easily read and load the parameters.

### Default Constants
After the physicsvalues module is imported, the default constants can easily be loaded:

```python
import physicsvalues as pvl

constants = pvl.default_constants
```

The `physicsvalues.default_constants` object is an ***immutable*** Dictionary class object - an extension of python dictionary, that allows addressing its keys via dot notation. The object contains all of the information from the `connstants.xml` file. The information in the object can be read as either a nested dictionary object with keys or with dot notation, where the key/attribute names match the element names in the `connstants.xml` file. For example:

```python
constants = pvl.default_constants

# get the speed of light value with dictionary keys
c = constants['physics_params']['speed_of_light']['value']

# get the speed of light value with dot notation
c = constants.physics_params.speed_of_light.value

```

Any additional information for the constants/parameters can be retrieved. For example:

```python
constants = pvl.default_constants

# get the source of the mean DM velocity value
print(constants.halo_params.mean_DM_velocity.source)

# print all of the information contained in constants
print(constants)

```

The `physicsvalues.default_constants` object is an ***immutable*** Dictionary class object. This means that keys, attributes, or item cannot be added, changed, or set. Attempting to modify the `physicsvalues.default_constants` object will result in an error.


### Load Constants Function
The constants and parameters can additionally be loaded using the `load_constants` function. The object return is a Dictionary class - an extension of python dictionary, that allows addressing its keys via dot notation.

**Parameters**

  - xml_path : str, optional
    - path to the xml file. If not provided, default constants are loaded. Use physicsvalues.default_constants, if default xml file is desired.
  - immutable : bool, optional
    - flag to decide whether or not to create a Dictionary class that is immutable (meaning items/attributes cannot be set). Used when loading default_constants.

**Returns**
  - Dictionary
    - Dictionary, containing elements and subelements that correspond to the elements of the xml file.
    
The `load_constants` function allows the user to load values from a custom xml file. As long as `immutable = False`, `load_constants` will return a Dictionary class object that is ***not immutable***. Unlike the the `physicsvalues.default_constants` object, the items of the object loaded using `load_constants` can be modified or additional items added. This is particularly useful if the user would like to see the effects caused by changing the value or a particular parameter. For example:

```python 
import physicsvalues as pvl

# load the constants

myconstants = pvl.load_constants()

# view all the contents in this object
print(myconstants)

# set a new value for the Eg10 parameter.
myconstants.Si_params.bandgap_params.Eg10.value = 1.2

```

---

## Project Status <a name="status"></a>
Potential or known updates include:

- Adding more constants to the xml file. This xml file is not a complete set of constants and parameters. We anticipate more constants to be added.
- Loading functions for other programming languages. In the case where there is demand for creating loading functions in different languages, a developer may create scripts to support this.

---

## Issue Tracking <a name="issue_tracking"></a>
For reporting issue with this package or requesting features, please submit a new issue for this project on GitLab. Unfortunately, merge requests are not currently supported for public users of this package. If you are a public user of this package and would like to submit a merge request, please contact the project manager.
