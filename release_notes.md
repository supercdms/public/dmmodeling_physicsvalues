# DM Modeling Physics Values Package - Release Notes
Contact: Taylor Aralis (taralis@caltech.edu), Matt Wilson (matthew.wilson@kit.edu)

Record of releases and tags for the physicsvalues repository and package. The tagging procedure follows the [Semantic Versioning](https://semver.org/) method.

---

## v2.2.5
**Date:** September 1, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Updated README and installation instructions based on the new name of the public version of this package. Updated name and url in setup file.

**Changes from Last Release Version:** No change to functionality.

---

## v2.2.4
**Date:** August 15, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Updates based on ORC review process for this software package.

**Changes from Last Release Version:** numpy packages added in install_requires. Fixed typos in README and added additional information. Added statement in LICENSE. Added sources for kgperamu and DM_density parameters in constants.xml. Updated source information in some Si bandgap_params and Si indirect_absorption_model_params.

---

## v2.2.3
**Date:** April 26, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Updated URL in setup.py file.

**Changes from Last Release Version:** No change to functionality.

---

## v2.2.2
**Date:** April 26, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Added source information for physics params in constants.xml file. Added source information for Si density and Eeh param in Si_params. Adjusted some parameter valves to match source more precisely. Adjustments are on the sub-percentage level.

**Changes from Last Release Version:** No change to functionality. Exact values of planck constant, reduced planch constant, and alpha parameter are adjusted slightly to match source value.

---

## v2.2.1
**Date:** Nov 18, 2022

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Added sentence in README that says this packages is used by SuperCDMS.


**Changes from Last Release Version:** No change to functionality.

---

## v2.2.0
**Date:** Feb 9, 2023

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Changed constants object to be a Dictionary class object - similar to python dictionary but can use dot notation. Created a defaul_constants object which is an immutable Dictionary class object (items connot be changed or set). Removed functions except for load_constants function and simplified the function. Now all information in the xml are loaded when load_constants is called. 

The constants.xml file has been restructured. Now each constant/parameter element contains additional child elements for the various information (e.g. value, units, source, etc..). For values that are either numbers or arrays, the attribtue 'type="float"' or 'type="array"' is added so that the load_constants() function knows how to treat the input.

License file also added. 


**Changes from Last Release Version:**
  - Removed all functions except for load_constants().
  - Constants object is now Dictionary class, similar to python dictionary but can use dot notation.
  - Created a default_constants object, which is an immutable Dictionary class.
  - No longer separate supplemental file or folder containing the covariance matrix information for the indirect Si absorption model. That information has been added to the constants.xml file.
  - The constants.xml file has been restructured to use child elements for each parameter/constant to provide additional information instead of attributes.
  

---

## v2.1.3
**Date:** Nov 18, 2022

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Added version.py file to define version field when package is installed. Setup file reads version from this python file.


**Changes from Last Release Version:** No change to functionality.

---

## v2.1.2
**Date:** Nov 14, 2022

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Fixed bug in indirect absorption in Si covariance matrix xml file.


**Changes from Last Release Version:** No change to functionality.

---

## v2.1.1
**Date:** Nov 14, 2022

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Fixed bug in setup.py file regarding data_files.


**Changes from Last Release Version:** No change to functionality.

---

## v2.1.0
**Date:** Nov 14, 2022

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Added additional file for covariance matrix of the indirecto absorption model in Si fit parameters. Added covar attribute to those parameters. Modified best fit parameters for indirect absorption model bast on new fitting procedure.


**Changes from Last Release Version:** Best fit parameters for indirect absorption model in Si modified. New 'physicsvalues/supplemental_files directory'. print_constants and load_single_constant functions can return covar path.

---

## v2.0.0
**Date:** Oct 24, 2022

**Author and Email:** Matt Wilson (matthew.wilson@kit.edu)

**Comments:** Restructured xml file to be nested elements based on categories of constants and parameters. Updated loader function to return a phython class object instead of a list of floats or strings. Added a loader function to return the value or attribute of a single parameter or constant. Added a print function to print out the names or additional details of constants and parameters. This version **is not backward compatible** with v1.0.0.


**Changes from Last Release Version:** Restructured xml file to be nested elements, updated loader function return phython class object, added function to load a single constant, added function to print out the names and details of constants, updated the README file with descriptions and examples, added to the xml file various constants and parameters that are required for the Si indirect absorption model.

---

## v1.0.0
**Date:** June 23, 2022

**Author and Email:** Taylor Aralis (taralis@caltech.edu)

**Comments:** First version of this package and repository. Added values for HVeVR2 reproduction. Added a loader function for python.


**Changes from Last Release Version:** N/A