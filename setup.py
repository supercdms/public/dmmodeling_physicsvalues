from setuptools import setup, find_packages
import re
import subprocess

# remove files that may cause installation problems. They are not needed for using the package
def clean_package(directories):
    for directory in directories:
        try:
            subprocess.run(["rm","-rf",directory])
        except:
            pass
clean_package(["build","dist","*.egg-info"])

# get the version from the version file
VERSIONFILE="physicsvalues/_version.py"
verstrline = open(VERSIONFILE, "rt").read()
VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
mo = re.search(VSRE, verstrline, re.M)
if mo:
    verstr = mo.group(1)
else:
    raise RuntimeError("Unable to find version string in %s." % (VERSIONFILE,))

# read the contents of your README file
with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name="dmmodeling_physicsvalues",
    version=verstr,
    long_description=long_description,
    author="Matthew Wilson",
    author_email="matthew.wilson@kit.edu",
    url="https://gitlab.com/supercdms/public/dmmodeling_physicsvalues",
    packages=find_packages(),
    install_requires=['numpy',
    ],
data_files=[
    'physicsvalues/constants.xml',
    ],
    include_package_data=True,
    python_requires=">=3",
)