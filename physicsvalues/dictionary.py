import logging

class Dictionary(dict):
    '''
    An extension of python dictionary that allows addressing its keys
    (as well as assigning new keys) via the dot notation.
    The usual python dict functionality works as well.
    NOTE: the dot notation only works for the keys which start from a
    letter or an underscore. E.g., d.key works, while d.1st_key doesn't.
    Such keys can only be addressed with the [] operator: d['1st_key'].
    '''
    def __getattr__(self, name):
        return self[name]
    
    def __setattr__(self, name, value):
        self.__setitem__(name, value)
        
    def __setitem__(self, name, value):
        if isinstance(value, dict) and not isinstance(value, Dictionary):
            value = Dictionary.deep_convert(value)
        dict.__setitem__(self, name, value)
        
    def __str(self, indentation=0):
        '''
        Private method to convert a Dictionary to a string with indentations
        of nested Dictionaries
        '''
        output = ''
        indentation_str = '\t'*indentation
        for k in self:
            key_str = f"'{k}'" if isinstance(k, str) else str(k)
            output += f'{indentation_str}{key_str}:'
            if isinstance(self[k], Dictionary):
                output += f'\n'+self[k].__str(indentation+1)
            else:
                output += f" {self[k]}\n"
        return output
        
    def __str__(self):
        return self.__str()
    
    def __repr__(self):
        return object.__repr__(self)+'\n\n'+self.__str()
    
    @staticmethod
    def deep_convert(d):
        '''
        Recursively converts a dict to a Dictionary. Checks if any of the
        items of the input is a dict and converts it to Dictionary
        
        Parameters
        ----------
        d : dict or Dictionary
            a dictionary to convert
            
        Returns
        -------
        Dictionary
        '''
        if isinstance(d, dict) and not isinstance(d, Dictionary):
            d = Dictionary(d)
        for key,value in d.items():
            if isinstance(value, dict):
                d[key] = Dictionary.deep_convert(value)
        return d

class ImmutableDictionary(dict):
    '''
    An extension of python dictionary that allows addressing its keys
    (as well as assigning new keys) via the dot notation.
    This Dictionary class is immutable, meaning attributes/items cannot be set.
    Used mainly to define the default_constants object.
    The usual python dict functionality works as well.
    NOTE: the dot notation only works for the keys which start from a
    letter or an underscore. E.g., d.key works, while d.1st_key doesn't.
    Such keys can only be addressed with the [] operator: d['1st_key'].
    '''
    def __getattr__(self, name):
        return self[name]
    
    def __setattr__(self, name, value):
        logging.warning("WARNING: New value or attribute cannot be set because "+\
                        "this ImmutableDictionary class is read only.")
        pass
        
    def __setitem__(self, name, value):
        logging.warning("WARNING: New value or attribute cannot be set because "+\
                        "this ImmutableDictionary class is read only.")
        pass
        
    def __str(self, indentation=0):
        '''
        Private method to convert a Dictionary to a string with indentations
        of nested Dictionaries
        '''
        output = ''
        indentation_str = '\t'*indentation
        for k in self:
            key_str = f"'{k}'" if isinstance(k, str) else str(k)
            output += f'{indentation_str}{key_str}:'
            if isinstance(self[k], ImmutableDictionary):
                output += f'\n'+self[k].__str(indentation+1)
            else:
                output += f" {self[k]}\n"
        return output
        
    def __str__(self):
        return self.__str()
    
    def __repr__(self):
        return object.__repr__(self)+'\n\n'+self.__str()
