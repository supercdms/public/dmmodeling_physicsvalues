import xml.etree.ElementTree as ET
import pkg_resources
import numpy as np
from .dictionary import *

__all__ = [
    "load_constants",
    "default_constants"
] 

constants_xml_path = pkg_resources.resource_filename(__name__, 'constants.xml')
constants_xml = ET.parse(constants_xml_path).getroot()

def parse_xml(element,immutable):
    '''
    Recursively parses an object of xml.etree.ElementTree.Element into a Dictionary
    
    Parameters
    ----------
    element : xml.etree.ElementTree.Element
        an object of xml.etree.ElementTree.Element to parse
    immutable : bool, optional
        flag to decide whether or not to create a Dictionary class that is immutable
        (meaning items/attributes cannot be set). Used when loading default_constants.
    
    Returns
    -------
    str, Dictionary
        a tuple of the root tag and a recursive dictionary with its elements.
    '''
    subelements = list(element)
    if subelements:
        if immutable:
            return element.tag, ImmutableDictionary([parse_xml(el,immutable) for el in subelements])
        else:
            return element.tag, Dictionary([parse_xml(el,immutable) for el in subelements])
    else:
        attributes = element.attrib
        value = element.text.strip()
        if 'type' in attributes:
            if attributes['type']=='float':
                value = float(value)
            elif attributes['type']=='int':
                value = int(value)
            elif attributes['type']=='str' or attributes['type']=='string':
                pass
            elif attributes['type']=='array':
                try:
                    value = np.fromstring(value, sep=',')
                    if 'shape' in attributes:
                        shape = np.fromstring(attributes['shape'], sep=',', dtype=int)
                        value = value.reshape(shape)
                except Exception as e:
                    raise Exception(f'Error when trying to parse the "{element.tag}" XML element.') from e
            else:
                raise Exception(f'Wrong "type" attribute in the "{element.tag}" XML element: {attributes["type"]}.'+\
                                'Allowed types are: "float", "int", "array", "str", "string".')
        return element.tag, value
    
def load_constants(xml_path=None,immutable=False):
    '''
    Loads constants from an xml file. Returns a Dictionary - an extension
    of python dictionary, that allows addressing its keys via dot notation.
    
    Parameters
    ----------
    xml_path : str, optional
        path to the xml file. If not provided, default constants are loaded.
        Use physicsvalues.default_constants, if default xml file is desired.
    immutable : bool, optional
        flag to decide whether or not to create a Dictionary class that is immutable
        (meaning items/attributes cannot be set). Used when loading default_constants.
    
    Returns
    -------
    Dictionary
        Dictionary, containing elements and subelements that correspond to
        the elements of the xml file.
    '''
    xml = constants_xml if xml_path is None else ET.parse(xml_path).getroot()
    return parse_xml(xml,immutable)[1]

default_constants = load_constants(immutable=True)